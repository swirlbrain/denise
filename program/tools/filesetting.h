
#pragma once

#include "../program.h"

struct FileSetting {
    
    FileSetting(GUIKIT::Settings* useSettings = nullptr) {
        
        if (!useSettings)
            this->useSettings = globalSettings;
        else
            this->useSettings = useSettings;
    }
    
    GUIKIT::Settings* useSettings;
	static std::vector<FileSetting*> instances;	
	std::string ident;
    Emulator::Interface* emulator = nullptr;
	
	std::string path = "";
	std::string file = "";
	unsigned id = 0;
	bool writeProtect = false;
	
	auto update() -> void {		
		this->path = useSettings->get<std::string>(ident + "_path", "");
		this->file = useSettings->get<std::string>(ident + "_file", "");
		this->id = useSettings->get<unsigned>(ident + "_id", 0);
		this->writeProtect = useSettings->get<bool>(ident + "_wp", false);
	}

	static auto getInstance(Emulator::Interface* emulator, std::string ident) -> FileSetting* {
		for (auto& instance : instances) {
			if ( (emulator == instance->emulator) && (ident == instance->ident) )
				return instance;			
		}
		auto instance = new FileSetting( program->getSettings( emulator ) );
		instance->ident = ident;
        instance->emulator = emulator;
		instance->update();
		instances.push_back(instance);

		return instance;
	}
	
	auto setPath(std::string value) -> void {
        if (value == "")
            useSettings->remove( ident + "_path" );
        else
            useSettings->set<std::string>(ident + "_path", value);
        
		this->path = value;
	}
	
	auto setFile(std::string value) -> void {
        if (value == "")
            useSettings->remove( ident + "_file" );
        else
            useSettings->set<std::string>(ident + "_file", value);
        
		this->file = value;
	}

	auto setId(unsigned value) -> void {
        if (value == 0)
            useSettings->remove( ident + "_id" );
        else
            useSettings->set<unsigned>(ident + "_id", value);
        
		this->id = value;
	}

	auto setWriteProtect(bool value) -> void {
        if (!value)
            useSettings->remove( ident + "_wp" );
        else
            useSettings->set<bool>(ident + "_wp", value);
        
		this->writeProtect = value;
	}        
	    
    auto setSaveable(bool state, bool neverSaveWriteProtection = false) -> void {
        useSettings->setSaveable( ident + "_path", state );
        useSettings->setSaveable( ident + "_id", state );
        useSettings->setSaveable( ident + "_wp", neverSaveWriteProtection ? false : state );
    }
	
	auto init() -> void {
		setPath("");
		setFile("");
		setId(0);
		setWriteProtect(false);
	}
};
