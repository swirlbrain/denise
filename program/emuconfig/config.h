
#pragma once

struct Message;
struct InputMapping;
struct FileSetting;
struct FirmwareManager;

#include "../../guikit/api.h"
#include "../program.h"
#include "layouts/model.h"

namespace EmuConfigView {

struct TabWindow;
	
#include "../config/slider.h"
#include "layouts/system.h"
#include "layouts/states.h"
#include "layouts/border.h"
#include "layouts/video.h"
#include "layouts/input.h"
#include "layouts/firmware.h"
#include "layouts/palette.h"
#include "layouts/misc.h"
#include "layouts/audio.h"

struct TabWindow : public GUIKIT::Window {
    
    enum Layout : unsigned { System, Control, States, Presentation, Palette, Audio, Firmware, Border, Misc };
    
    Emulator::Interface* emulator;
    
    Message* message;
    InputLayout* inputLayout = nullptr;
    SystemLayout* systemLayout = nullptr;
    AudioLayout* audioLayout = nullptr;
    FirmwareLayout* firmwareLayout = nullptr;
    BorderLayout* borderLayout = nullptr;
    VideoLayout* videoLayout = nullptr;
    PaletteLayout* paletteLayout = nullptr;
    StatesLayout* statesLayout = nullptr;
    MiscLayout* miscLayout = nullptr;
    GUIKIT::Settings* settings = nullptr;

    GUIKIT::TabFrameLayout tab;
    
    GUIKIT::Image joystickImage;
    GUIKIT::Image systemImage;
    GUIKIT::Image memoryImage;
    GUIKIT::Image cropImage;
    GUIKIT::Image displayImage;
    GUIKIT::Image scriptImage;    
    GUIKIT::Image paletteImage;
    GUIKIT::Image volumeImage;
	
	GUIKIT::Timer mtimer;

    auto build() -> void;	
    auto translate() -> void;
    auto show(Layout layout) -> void;
	auto showDelayed(Layout layout) -> void;
	static auto getView( Emulator::Interface* emulator ) -> TabWindow*;

    TabWindow(Emulator::Interface* emulator);
};

}

extern std::vector<EmuConfigView::TabWindow*> emuConfigViews;
