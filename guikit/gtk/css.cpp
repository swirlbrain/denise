
	std::string css = R"(
		
		checkbutton {
			padding-left: 0px;
			padding-right: 0px;
			padding-top: 0px;
			padding-bottom: 0px;		
		}	
		
		checkbutton > check {
			min-width: 16px;
			min-height: 16px;
			margin-left: 0px;
			margin-right: 4px;
			margin-top: 0px;
			margin-bottom: 0px;		
		}
		
		checkbutton > label {
			padding: 0px;
			margin: 0px;
		}		
		
		radiobutton {
			padding-left: 0px;
			padding-right: 0px;
			padding-top: 0px;
			padding-bottom: 0px;		
		}
		
		radiobutton > radio {
			min-width: 16px;
			min-height: 16px;
			margin-left: 0px;
			margin-right: 4px;
			margin-top: 0px;
			margin-bottom: 0px;		
		}
		
		radiobutton > label {
			padding: 0px;
			margin: 0px;
		}	
		
		combobox {
			padding-left: 0px;
			padding-right: 0px;
			padding-top: 0px;
			padding-bottom: 0px;
		}
		
		combobox button {
			padding-left: 2px;
			padding-right: 2px;
			padding-top: 0px;
			padding-bottom: 0px;
		}
		
		entry {
			min-height: 0px;
			padding-left: 3px;
			padding-right: 3px;
			padding-top: 5px;
			padding-bottom: 5px;
		}	
		
		frame {
			padding: 0px;
		}
		
		label {
			padding: 0px;
		}
		
		notebook {
			padding: 0px;
			border-width: 1px;
			border-style: solid;
		}
		
		notebook tab  {
			min-height: 25px;
		}
		
		statusbar {
			background-color: white;
		}
		
		.paddingMenuItem {
			padding-left: 25px;
		}

		scale {
			padding: 3px 0px;
		}
		
		scrolledwindow undershoot.top, scrolledwindow undershoot.right, scrolledwindow undershoot.bottom, scrolledwindow undershoot.left {
			background-image: none;
		}
		
	)";