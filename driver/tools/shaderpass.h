
#pragma once

struct CropPass {        
    unsigned top = 0;
    unsigned left = 0;
    unsigned bottom = 0;
    unsigned right = 0;
    
    bool active = false;
    
    auto release() -> void {
        top = left = bottom = right = 0;
        active = false;
    }
    
    auto set(CropPass& crop) -> void {
        top = crop.top;
        left = crop.left;
        bottom = crop.bottom;
        right = crop.right;
        active = top || left || bottom || right;
    }
};

struct ShaderPass {	

    CropPass crop;
    
	bool primary = false;
    bool internalFormatMatchesData = false;
    bool external = true;
    bool mipmap = false;
	
	std::string fragment = "";	//fragment, pixel or as effect (any shader)
	std::string vertex = "";	//vertex
	std::string geometry = "";	//geometry

	std::string filter = "";
	std::string wrap = "";
	std::string format = "";

	unsigned relativeWidth = 0;
	unsigned relativeHeight = 0;
	unsigned modulo = 0;    

    std::string error = "";
    std::string ident = "";
    unsigned program = 0;
    
    ShaderPass() {}
    
    ShaderPass(const ShaderPass& source) {
        operator=(source);
    }

    ShaderPass& operator=(const ShaderPass& source) {
        if(this == &source)
            return *this;

        primary = source.primary;     
        internalFormatMatchesData = source.internalFormatMatchesData;
        fragment = source.fragment;
        vertex = source.vertex;
        geometry = source.geometry;
        filter = source.filter;
        wrap = source.wrap;                
        format = source.format;
        relativeHeight = source.relativeHeight;
        relativeWidth = source.relativeWidth;
        modulo = source.modulo;
        error = source.error;
        ident = source.ident;
        program = source.program;
        crop = source.crop;
        external = source.external;
        mipmap = source.mipmap;

        return *this;
    }
    
};
