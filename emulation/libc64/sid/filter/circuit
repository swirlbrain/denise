
//  This code is a modification of the resid engine in VICE
//  You can get a copy of the original here: https://sourceforge.net/projects/vice-emu/

//  ---------------------------------------------------------------------------
//  This file is part of VICE, the Versatile Commodore Emulator.
//  This file is part of reSID, a MOS6581 SID emulator engine.
//  Copyright (C) 2010  Dag Lem <resid@nimrod.no>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//  ---------------------------------------------------------------------------

SID 6581 filter / mixer / output
--------------------------------

                ---------------------------------------------------
               |                                                   |
               |                         --1R1-- \--  D7           |
               |              ---R1--   |           |              |
               |             |       |  |--2R1-- \--| D6           |
               |    ------------<A]-----|           |     $17      |
               |   |                    |--4R1-- \--| D5  1=open   | (3.5R1)
               |   |                    |           |              |
               |   |                     --8R1-- \--| D4           | (7.0R1)
               |   |                                |              |
 $17           |   |                    (CAP2B)     |  (CAP1B)     |
 0=to mixer    |    --R8--    ---R8--        ---C---|       ---C---| 
 1=to filter   |          |  |       |      |       |      |       |
                ------R8--|-----[A>--|--Rw-----[A>--|--Rw-----[A>--|
     ve (EXT IN)          |          |              |              |
 D3  \ ---------------R8--|          |              | (CAP2A)      | (CAP1A)
     |   v3               |          | vhp          | vbp          | vlp
 D2  |   \ -----------R8--|     -----               |              |
     |   |   v2           |    |                    |              |
 D1  |   |   \ -------R8--|    |    ----------------               |
     |   |   |   v1       |    |   |                               |
 D0  |   |   |   \ ---R8--     |   |    ---------------------------
     |   |   |   |             |   |   |
     R6  R6  R6  R6            R6  R6  R6
     |   |   |   | $18         |   |   |  $18
     |    \  |   | D7: 1=open   \   \   \ D6 - D4: 0=open
     |   |   |   |             |   |   |
      ---------------------------------                          12V
                 |
                 |               D3  --/ --1R2--                  |
                 |    ---R8--       |           |   ---R2--       |
                 |   |       |   D2 |--/ --2R2--|  |       |  ||--
                  ------[A>---------|           |-----[A>-----||
                                 D1 |--/ --4R2--| (4.25R2)    ||--
                        $18         |           |                 |
                        0=open   D0  --/ --8R2--  (8.75R2)        |
                                                                  vo (AUDIO
                                                                      OUT)																	 OUT)

 v1  - voice 1
 v2  - voice 2
 v3  - voice 3
 ve  - ext in
 vhp - highpass output
 vbp - bandpass output
 vlp - lowpass output
 vo  - audio out
 [A> - single ended inverting op-amp (self-biased NMOS inverter)
 Rn  - "resistors", implemented with custom NMOS FETs
 Rw  - cutoff frequency resistor (VCR)
 C   - capacitor


 SID 8580 filter / mixer / output
 --------------------------------
 
               +---------------------------------------------------+
               |    $17      +----Rf-+                             |
               |             |       |                             |
               |      D4&D5  o- \-R3-o                             |
               |             |       |                    $17      |
               |     !D4&D5  o- \-R2-o                             |
               |             |       |  +---R8-- \--+  !D6&D7      |
               |      D4&!D5 o- \-R1-o  |           |              |
               |             |       |  o---RC-- \--o   D6&D7      |
               |   +---------o--<A]--o--o           |              |
               |   |                    o---R4-- \--o  D6&!D7      |
               |   |                    |           |              |
               |   |                    +---Ri-- \--o !D6&!D7      |
               |   |                                |              |
 $17           |   |                    (CAP2B)     |  (CAP1B)     |
 0=to mixer    |   +--R8--+  +---R8--+      +---C---o      +---C---o
 1=to filter   |          |  |       |      |       |      |       |
               +------R8--o--o--[A>--o--Rfc-o--[A>--o--Rfc-o--[A>--o
     ve (EXT IN)          |          |              |              |
 D3  \ ---------------R8--o          |              | (CAP2A)      | (CAP1A)
     |   v3               |          | vhp          | vbp          | vlp
 D2  |   \ -----------R8--o    +-----+              |              |
     |   |   v2           |    |                    |              |
 D1  |   |   \ -------R8--o    |   +----------------+              |
     |   |   |   v1       |    |   |                               |
 D0  |   |   |   \ ---R8--+    |   |   +---------------------------+
     |   |   |   |             |   |   |
     R6  R6  R6  R6            R6  R6  R6
     |   |   |   | $18         |   |   |  $18
     |    \  |   | D7: 1=open   \   \   \ D6 - D4: 0=open
     |   |   |   |             |   |   |
     +---o---o---o-------------o---o---+
                 |
                 |               D3 +--/ --1R2--+
                 |   +---R8--+      |           |  +---R2--+
                 |   |       |   D2 o--/ --2R2--o  |       |
                 +---o--[A>--o------o           o--o--[A>--o-- vo (AUDIO OUT)
                                 D1 o--/ --4R2--o (4.25R2)
                        $18         |           |
                        0=open   D0 +--/ --8R2--+ (8.75R2)

Rfc - freq control DAC resistance ladder
